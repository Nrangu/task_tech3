﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace TechTask3
{
    public class ForHit : MonoBehaviour
    {

        #region private fields
        [SerializeField] private Character _character = null;
        #endregion

        #region public methods
        public int Level
        {
            get { return _character.Characteristics.Level; }
        }
        public int ObjectLayer
        {
            get { return _character.Layer; }
        }
        public bool OnHit(int damage)
        {
            return _character.Hit(damage);
        }
        #endregion
    }
}