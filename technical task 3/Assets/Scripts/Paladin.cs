﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace TechTask3
{
    public class Paladin : BaseSceneObject
    {
        #region private fields
        private CharacterActions _paladinActions = null;
        private Animator _animator;

        private Character _character = null;
        #endregion
        #region protected methods
        protected override void Awake()
        {
            base.Awake();
             _paladinActions = GetComponent<CharacterActions>();
            if (! _paladinActions)
            {
                 _paladinActions = InstanceObject.AddComponent<CharacterActions>();
            }

            _animator = GetComponent<Animator>();

            _character = GetComponent<Character>();
        }
        #endregion
        #region private methods
        // Use this for initialization
        void Start()
        {
             _paladinActions.SeActivateAction(CharacterStates.ATTACK, delegate () { if (_animator) { _animator.SetBool("Attack", true); } });
             _paladinActions.SeDeactivateAction(CharacterStates.ATTACK, delegate () { if (_animator) { _animator.SetBool("Attack", false); } });

             _paladinActions.SeActivateAction(CharacterStates.DEFENCE, delegate () { if (_animator) { _animator.SetBool("Defence", true); } });
             _paladinActions.SeDeactivateAction(CharacterStates.DEFENCE, delegate () { if (_animator) { _animator.SetBool("Defence", false); } });

             _paladinActions.SeActivateAction(CharacterStates.WALK, delegate () { if (_animator) { _animator.SetBool("Walk", true); } });
             _paladinActions.SeDeactivateAction(CharacterStates.WALK, delegate () { if (_animator) { _animator.SetBool("Walk", false); } });

             _paladinActions.SeActivateAction(CharacterStates.WALKBACKWARDS, delegate () { if (_animator) { _animator.SetBool("WalkBackwards", true); } });
             _paladinActions.SeDeactivateAction(CharacterStates.WALKBACKWARDS, delegate () { if (_animator) { _animator.SetBool("WalkBackwards", false); } });

             _paladinActions.SeActivateAction(CharacterStates.DEAD, delegate () { if (_animator) { _animator.SetBool("Dead", true); } });
        }
        void OnEndAttack()
        {
            _paladinActions.CharacterState = CharacterStates.IDLE;
        }
        #endregion
        #region public methods

        #endregion
    }
}
