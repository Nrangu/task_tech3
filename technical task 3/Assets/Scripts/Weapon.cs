﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace TechTask3
{
    /// <summary>
    /// Класс для описания оружия
    /// </summary>

    //Думаю атаку нужно брать из класса оружия.
    // И передавать значения атаки персонажу
    public class Weapon : BaseSceneObject
    {

        #region private fields
        [SerializeField] private Character _character;
        private event dlgForHit Ev;
        #endregion
        #region private methods
        // Use this for initialization
        void Start()
        {
            if (Ev != null)
            {

            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            if (!Helpers.inLayer(1 << other.gameObject.layer, _character.EnemyLayers))
            {
                return;
            }

            if (_character.CharacterState != CharacterStates.ATTACK) return;

                ForHit enemy = other.gameObject.GetComponent<ForHit>();

            if (enemy == null) return;

            
                _character.OnHit( enemy);

            /*
            // сохраняем уровень сразу, так как при переходе
            // в состояние смерти враг объет может быть уничтожен
            int enemyLevel = _enemy.Level;
            
            // проверяем не привела ли атака к смерти врага
            if (enemy.OnHit(_character.Attack()) )
            {
                _character.Characteristics.Eperiance += enemyLevel * _character.Characteristics.DeltaExpereanceForKill;
            };
            */
        }
        #endregion
        #region public methods
        #endregion
    }
}