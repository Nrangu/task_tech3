﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;
namespace TechTask3 {
    public class EnemyCanvas : BaseGUI {

        #region private fields
        [SerializeField] private Camera _camera = null;
        #endregion
        #region private methods
        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {
            transform.LookAt(transform.position + _camera.transform.rotation * Vector3.forward, _camera.transform.rotation * Vector3.up);

        }
        #endregion
    }
}