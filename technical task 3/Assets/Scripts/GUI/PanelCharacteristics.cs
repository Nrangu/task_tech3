﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace TechTask3
{

    public class PanelCharacteristics : BaseGUI
    {
        #region private fields

        [SerializeField] private Character _character;
        private Characteristics _characteristics = null;
        [SerializeField] private BaseGUIText _level;
        [SerializeField] private BaseGUIText _experiance;
        [SerializeField] private BaseGUIText _baseForce;
        [SerializeField] private BaseGUIText _baseStamina;
        [SerializeField] private BaseGUIText _baseIntelligence;
        [SerializeField] private BaseGUIText _skillPoints;

        [SerializeField] private BaseGUIText _attack;
        [SerializeField] private BaseGUIText _defence;
        [SerializeField] private BaseGUIText _health;
        [SerializeField] private BaseGUIText _stamina;
        [SerializeField] private BaseGUIText _mana;


        #endregion
        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            ActiveObject = false;
            if (_character)
            {
                _characteristics = _character.Characteristics;
            }
        }
        #endregion

        #region private methods
        private void Start()
        {

        }
        private void Update()
        {
        }
        private void OnEnable()
        {
            InitData();
        }
        private void InitData()
        {
            if (_characteristics != null)
            {
                _level.Text = _character.Characteristics.Level.ToString();
                _experiance.TextLeft = _character.Characteristics.Eperiance.ToString() + "/";
                _experiance.TextRight = _character.Characteristics.NeedExperiance.ToString();
                _baseForce.Text = _characteristics.BaseForce.ToString();
                _baseStamina.Text = _characteristics.BaseStamina.ToString();
                _baseIntelligence.Text = _characteristics.BaseIntelligence.ToString();
                _skillPoints.Text = _characteristics.SkillPoints.ToString();
                _attack.TextLeft = _characteristics.MinAttack.ToString() + " - ";
                _attack.TextRight = _characteristics.MaxAttack.ToString();
                _defence.Text = _characteristics.Defence.ToString();
                _health.Text = _characteristics.MaxHealth.ToString();
                _stamina.Text = _characteristics.MaxStamina.ToString();
                _mana.Text = _characteristics.MaxMana.ToString();
            }

        }
        #endregion
        #region public methods
        public void OnClickUpBaseForce()
        {
            if (_characteristics.SkillPoints > 0)
            {
                _characteristics.SkillPoints -= 1;
                _characteristics.BaseForce += 1;
                InitData();
            }
        }

        public void OnClickUpBaseStamina()
        {
            if (_characteristics.SkillPoints > 0)
            {
                _characteristics.SkillPoints -= 1;
                _characteristics.BaseStamina += 1;
                InitData();
            }

        }

        public void OnClickUpBaseIntelligence()
        {
            if (_characteristics.SkillPoints > 0)
            {
                _characteristics.SkillPoints -= 1;
                _characteristics.BaseIntelligence += 1;
                InitData();
            }

        }

        #endregion
    }
}