﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace TechTask3
{
    /// <summary>
    /// Активация / деактивация  массивов объектов
    /// </summary>
    public class GUIOnOff :BaseGUI
    {
        #region private fields;
        // объеты интерфейса которые нужно показать при нажатии кнопки
        [SerializeField] private BaseGUI[] _activate = null;

        // объекты интерфейса которые нужно спрятать при нажатии кнопки
        [SerializeField] private BaseGUI[] _deActivate = null;
        #endregion
        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            ActiveObject = true;
        }
        #endregion
        #region public methods
        public void OnDo()
        {
            foreach( BaseGUI guiObject in _deActivate)
            {
                guiObject.ActiveObject = false;
            }

            foreach (BaseGUI guiObject in _activate)
            {
                guiObject.ActiveObject = true;
            }
        }
        #endregion


    }
}