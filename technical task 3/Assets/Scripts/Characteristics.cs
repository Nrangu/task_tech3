﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TechTask3
{
    /// <summary>
    /// Класс для описания и работы с характеристиками персонажа
    /// </summary>
    /// 

        // Есть много свойств которые будут заданы один раз при старте,
        // а потом они изменяются автоматически. 
        // Это можно сделать либо через конструктор
        // либо через метод инициализации
        // Пока оставил возможность менять через свойства, что бы можно было работать
    public class Characteristics
    {
        public delegate void ForEvent();
        #region private fields
        // Предположим что уровень не будет больше 255
        // если будет, тогда нужно буде изменить тип
        private byte _level = 0;
        // текущее значение опыта
        private int _experiance = 0;
        // количество опыта необходимое для перехода
        // на следующий уровень.
        private int _needExperiance = 0;
        // коэфициент увеличения нужного опыта 
        // при увеличении уровня
        private float _deltaExperiance = 1;
        
        //коєфициент опыта за уровень при убийстве врага
        // уровень = 2 колличесвто опыта будет уровень * _deltaExp;
        private int _deltaExpereanceForKill = 1;

        //базовый коэфициент защиты, даже если защита 0
        private float _deltaDefence = 1;

        // очки умений необходимые для прокачки героя
        private int _skillPoints;
        // сколько очков получает персонаж при  увеличение уровня
        private int _deltaSkillPoints;

        // базавые характеристики
        // те которые можно будет увеличить при повышении уровня
        // используя очки улучшения 
        private int _baseForce = 0;
        private int _baseStamina = 0;
        private int _baseIntelligence = 0;

        //Характеристики зависящие от экипировки
        private int _minAttack = 0;
        private int _maxAttack = 0;
        private int _defence = 0;

        // характеристики которые нельзя изменить напрямую
        // они связаны с базовыми характеристиками, 
        // и изменяются при изменении базовых характеристики
        //
        private int _maxHealth = 0;
        private int _maxStamina = 0;
        private int _maxMana = 0;

        // текущие значения здоровья, выносливости и маны
        private int _health = 0;
        private int _stamina = 0;
        private int _mana = 0;

        // Соотношения косвенных характеристик к  базовым
        // соотношение здоровья к базовой силе 
        private int _relationHealthBaseForce = 1;
        // соотношение выносливости к базовой выносливости
        private int _relationStaminaBaseStamina = 1;
        // соотношение маны к интелекту
        private int _relationManaBaseIntelligence = 1;
        #endregion
        #region public methods
        public event ForEvent OnLevelChange;
        public event ForEvent OnHealthChange;
        public event ForEvent OnManaChange;
        public event ForEvent OnStaminaChange;
        public event ForEvent OnMaxHealthChange;
        public event ForEvent OnMaxStaminaChange;
        public event ForEvent OnMaxManaChange;
        public Characteristics( byte level, 
                                int needExperiance, 
                                float deltaExperiance, 
                                int deltaSkillPoints, 
                                int baseForce, 
                                int baseStamina, 
                                int baseIntelligence, 
                                int relationHealthBaseForce,
                                int relationStaminaBaseStamina,
                                int relationManaBaseIntelligence,
                                float deltaDefence,
                                int deltaExpForKill)
        {
            DeltaSkillPoints = deltaSkillPoints;
            Level = level;

            SkillPoints = 12;

            NeedExperiance = needExperiance;
            DeltaExpeiance = deltaExperiance;
            DeltaDefence = deltaDefence;

            RelationHealthBaseForce = relationHealthBaseForce;
            RelationManaBaseIntelligence = relationManaBaseIntelligence;
            RelationStaminaBaseStamina = relationStaminaBaseStamina;

            BaseForce = baseForce;
            BaseStamina = baseStamina;
            BaseIntelligence = baseIntelligence;
            Health = MaxHealth;
            Mana = MaxMana;
            Stamina = MaxStamina;
            DeltaExpereanceForKill = deltaExpForKill;

        }
        #endregion
        #region private methods

        // так минимальная атака не может быть 
        // больше максимальной сделаем проверку 
        // И вызывать этот метод нужно ТОЛЬКо после 
        // изменения обоих параметров
        private void MinMaxAttack()
        {
            if ( _minAttack > _maxAttack)
            {
                int tmp = _minAttack;

                _minAttack = _maxAttack;
                _maxAttack = tmp;
            }
        }
        #endregion
        #region public properties
        public int DeltaExpereanceForKill
        {
            get { return _deltaExpereanceForKill; }
            set
            {
                if (value > 0)
                {
                    _deltaExpereanceForKill = value;
                }else
                {
                    _deltaExpereanceForKill = 1;
                }
            }
        }
        public byte Level
        {
            get { return _level; }
            // Так как уровень понижать нельзя
            // сделаем проверку, что бы случайно не уменьшить уровень
            set
            {
                if (value > _level)
                {
                    _level = value;
                    SkillPoints += _deltaSkillPoints;
                    Health = MaxHealth;
                    Stamina = MaxStamina;
                    Mana = MaxMana;
                    if (OnLevelChange != null)
                    {
                        OnLevelChange();
                    }
                }
            }
        }

        public int SkillPoints
        {
            get { return _skillPoints; }
            set
            {
                if (value > 0)
                {
                    _skillPoints = value;
                }else
                {
                    _skillPoints = 0;
                }
            }
        }

        public int DeltaSkillPoints
        {
            get { return _deltaSkillPoints; }
            // так как нет смысла получать за уровень меньше одного очка
            // пусть минимум будт 1
            set
            {
                if ( value > 0)
                {
                    _deltaSkillPoints = value;
                }else
                {
                    _deltaSkillPoints = 1;
                }

            }
        }

        public int Eperiance
        {
            get { return _experiance; }
            // Вроде нельзя уменьшать опыт
            // так что сделаем проверку на уменьшение
            // Если вдруг нужно будет добавить возможность
            // уменьшать опыт(возможно после смерти героя)
            // нужно будет код изменить, либо написать
            // отдельный метод
            set
            {
                if (value > _experiance)
                {
                    _experiance = value;
                }else
                {
                    _experiance = 0;
                    return;
                }

                // цилично. проверяем больше ли текущее значение опыта чем нужно для повышения уровня
                // если да то повышаем уровень
                // запоминаем остаток опыта превышающий предел
                // высчитываем новый предел
                // Делаем это циклично, так как есть шанс получить опыта на несколько уровней
                while (_experiance >= _needExperiance)
                {
                    Level += 1;
                    _experiance = _experiance - _needExperiance;
                    _needExperiance += (int)(_needExperiance * _deltaExperiance); 

                }
            }
        }
        public float DeltaExpeiance
        {
            get { return _deltaExperiance; }
            // так как умножаем на этот параметр
            // то по умолчанию устанавливаем ему значение 1
            set
            {
                if (value > 0)
                {
                    _deltaExperiance = value;
                }else
                {
                    _deltaExperiance = 1;
                }
            }
        }

        public int NeedExperiance
        {
            get { return _needExperiance; }
            // нет смысла устанавливать количество опыта, необходимого для перехода
            // на новый уровень, меньше 1. 
            // Будем считать минимальным значением 1
            set
            {
                if (value > 0)
                {
                    _needExperiance = value;
                }else
                {
                    _needExperiance = 1;
                }
            }
        }

        public int BaseForce
        {
            get { return _baseForce; }
            set
            {
                if (value > _baseForce)
                {
                    _baseForce = value;
                    _maxHealth = _baseForce * _relationHealthBaseForce;
                    if (OnMaxHealthChange != null)
                    {
                        OnMaxHealthChange();
                    }
                    //_health = _maxHealth;
                }
            }
        }

        public int BaseStamina
        {
            get { return _baseStamina; }
            set
            {
                if (value > _baseStamina)
                {
                    _baseStamina = value;
                    _maxStamina = _baseStamina * _relationStaminaBaseStamina;
                    if (OnMaxStaminaChange != null)
                    {
                        OnMaxStaminaChange();
                    }
                   // _stamina = _maxStamina;
                }
            }
        }

        public int BaseIntelligence
        {
            get { return _baseIntelligence; }
            set
            {
                if (value > _baseIntelligence)
                {
                    _baseIntelligence = value;
                    _maxMana = _baseIntelligence * _relationManaBaseIntelligence;
                    if (OnMaxManaChange != null)
                    {
                        OnMaxManaChange();
                    }
                }
            }
        }

        public int MaxHealth
        {
            get { return _maxHealth; }
        }

        public int MaxStamina
        {
             get { return _maxStamina; }   
        }

        public int MaxMana
        {
            get { return _maxMana; }
        }

        public int Health
        {
            get { return _health; }
            set
            {
                if (value > 0)
                {
                    if (value > _maxHealth)
                    {
                        _health = _maxHealth;
                    }else
                    {
                        _health = value;
                    }
                }
                else
                {
                    _health = 0;
                }
                if (OnHealthChange != null)
                {
                    OnHealthChange();
                }
            }
        }

        public int Stamina
        {
            get { return _stamina; }
            set
            {
                if (value > 0)
                {
                    if (value > _maxStamina)
                    {
                        _stamina = _maxStamina;
                    }
                    else
                    {
                        _stamina = value;
                    }
                }else
                {
                    _stamina = 0;
                }
                if (OnStaminaChange != null)
                {
                    OnStaminaChange();
                }
            }
        }
        public int Mana
        {
            get { return _mana; }
            set
            {
                if ( value > 0)
                {
                    if (value > _maxMana)
                    {
                        _mana = _maxMana;
                    }
                    else
                    {
                        _mana = value;
                    }
                }else
                {
                    _mana = 0;
                }
                if (OnManaChange != null)
                {
                    OnManaChange();
                }
            }
        }

        public int MaxAttack
        {
            get { return _maxAttack; }
            set
            {
                if ( value > 0)
                {
                    _maxAttack = value;
                }else
                {
                    _maxAttack = 0;
                }
            }
        }

        public int MinAttack
        {
            get { return _minAttack; }
            set
            {
                if (value > 0)
                {
                    _minAttack = value;
                }else
                {
                    _minAttack = 0;
                }
            }
        }

        public int Defence
        {
            get { return _defence; }
            set
            {
                if (value > 0)
                {
                    _defence = value;
                } else
                {
                    _defence = 0;
                }
            }
        }

        public float DeltaDefence
        {
            get { return _deltaDefence; }
            set
            {
                if (value > 0)
                {
                    _deltaDefence = value;
                }else
                {
                    _deltaDefence = 1;
                }
            }
        }

        public int RelationHealthBaseForce
        {
            get { return _relationHealthBaseForce; }

            // так как происходит умножение этой характеристики
            // то проверку делаем на 1 и присваиваем 1
            set
            {
                if (value > 1)
                {
                    _relationHealthBaseForce = value;
                }else
                {
                    _relationHealthBaseForce = 1;
                }
            }
        }
        
        public  int RelationStaminaBaseStamina
        {
            get { return _relationStaminaBaseStamina; }
            set
            {
                if (value > 1)
                {
                    _relationStaminaBaseStamina = value;
                }else
                {
                    _relationStaminaBaseStamina = 1;
                }
            }
        }

        public int RelationManaBaseIntelligence
        {
            get { return _relationManaBaseIntelligence; }

            set
            {
                if (value > 1)
                {
                    _relationManaBaseIntelligence = value;
                }else
                {
                    _relationManaBaseIntelligence = 1;
                }
            }
        }


        #endregion
    }
}