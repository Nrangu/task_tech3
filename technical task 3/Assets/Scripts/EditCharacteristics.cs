﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TechTask3 {

    public class EditCharacteristics : MonoBehaviour {
        [SerializeField] private Character _character;
        public void OnButtonDown()
        {
            _character.Characteristics.BaseForce += 10;
        }
    }
}