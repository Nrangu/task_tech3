﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;
namespace TechTask3
{
    public class Enemy : BaseSceneObject
    {
        #region private fields
        [SerializeField] private int _minAttack = 0;
        [SerializeField] private int _maxAttack = 0;
        [SerializeField] private int _defence = 0;
        [SerializeField] private SliderLine _healthLine = null;
        [SerializeField] private BaseGUIText _enemyNameText = null;
        [SerializeField] private string _enemyName = "";
        [SerializeField] private EnemyCanvas _canvas = null;
        [SerializeField] private Ai_simple _AI = null;
        private CharacterActions _actions = null;
        private bool _finded = false;

        private Character _character = null;
        #endregion

        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            _actions = GetComponent<CharacterActions>();
            if (!_actions)
            {
                _actions = InstanceObject.AddComponent<CharacterActions>();
            }


            _character = GetComponent<Character>();
        }
        #endregion
        #region private methods
        // Use this for initialization
        void Start()
        {
            _character.Characteristics.MinAttack = _minAttack;
            _character.Characteristics.MaxAttack = _maxAttack;
            _character.Characteristics.Defence = _defence;

            _character.Characteristics.OnHealthChange += OnHealthChange;

            if (_healthLine)
            {
                _healthLine.MinValue = 0;
                _healthLine.MaxValue = _character.Characteristics.MaxHealth;
                _healthLine.Value = _healthLine.MaxValue;
            }
            _enemyNameText.Text = _enemyName;
            if (_canvas)
            {
                _canvas.ActiveObject = false;
            }
            if (_character)
            {
                _character.OnHitEvent += delegate (ForHit forHit)
                {
                    // проверяем не привела ли атака к смерти игрока
                    if (forHit.OnHit(_character.Attack()))
                    {
                        
                        _actions.CharacterState = CharacterStates.IDLE;
                    };

                };
            }
            _AI = GetComponent<Ai_simple>();
            _actions.CharacterState = CharacterStates.IDLE;
        }
        // Update is called once per frame
        void Update()
        {

            //_actions.CharacterState = CharacterStates.ATTACK;

            switch (_actions.CharacterState)
            {
                case CharacterStates.DEAD:
                    if (_canvas)
                    {
                        _canvas.ActiveObject = false;
                    }
                    break;
                    
            }
        }
        #endregion

        #region public methods

        public void OnHealthChange()
        {
            _healthLine.Value = _character.Characteristics.Health;
        }
        public void ObjectFinded(bool finded)
        {
            if (_finded == true && finded == true)
            {
                return;
            }
            _finded = finded;
            _canvas.ActiveObject = _finded;

        }
        #endregion
        #region public properties
        public bool Finded
        {
            get { return _finded; }
        }
        #endregion
    }
}
