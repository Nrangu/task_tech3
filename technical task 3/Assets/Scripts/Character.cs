﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace TechTask3
{
    public delegate void dlgForHit(ForHit _enemy);

    public class Character : BaseSceneObject
    {
        #region private fields;
        private Characteristics _characteristics;


        [Header("Начальные значения характеристик")]
        [SerializeField] private byte _level;
        [SerializeField] private int _needExperiance = 0;
        [SerializeField] private float _deltaExperiance = 0;
        [SerializeField] private int _deltaSkillPoints;
        [SerializeField] private int _baseForce = 0;
        [SerializeField] private int _baseStamina = 0;
        [SerializeField] private int _baseIntelligence = 0;
        [SerializeField] private int _relationHealthBaseForce = 1;
        [SerializeField] private int _relationStaminaBaseStamina = 1;
        [SerializeField] private int _relationManaBaseIntelligence = 1;
        [SerializeField] private float _deltaDefence = 1;
        [SerializeField] private float _moveSpeed = 0;
        [SerializeField] private float _rotateSpeed = 0;
        [SerializeField] private LayerMask _enemyLayers = 0;
        //коєфициент опыта за уровень при убийстве врага
        // уровень = 2 колличесвто опыта будет уровень * _deltaExp;
        [SerializeField] private int _deltaExpereanceForKill = 1;

        [SerializeField] private CharacterActions _actions;

        private CharacterController _characterController;
        private Vector3 _forward;
        public event dlgForHit OnHitEvent;
  

        #endregion
        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            //Helpers.inLayer(1 << this.gameObject.layer, _enemyLayers);
            _characterController = GetComponent<CharacterController>();

            _characteristics = new Characteristics(_level,
                                                   _needExperiance,
                                                   _deltaExperiance,
                                                   _deltaSkillPoints,
                                                   _baseForce,
                                                   _baseStamina,
                                                   _baseIntelligence,
                                                   _relationHealthBaseForce,
                                                   _relationStaminaBaseStamina,
                                                   _relationManaBaseIntelligence,
                                                   _deltaDefence,
                                                   _deltaExpereanceForKill);
        }
        #endregion
        #region private methods;
        void Dead()
        {
            _actions.CharacterState = CharacterStates.DEAD;
        }
        #endregion
        #region public methods
        public void OnHit(ForHit forHit)
        {
            if ( OnHitEvent != null)
            {
                OnHitEvent(forHit);
            }
        }
        public int Attack()
        {
            return Random.Range(_characteristics.MinAttack, _characteristics.MaxAttack + 1);
        }
        public void MoveForward(float param)
        {
            _forward = transform.TransformDirection(Vector3.forward);

            float curSpeed = _moveSpeed * param;
            _characterController.SimpleMove(_forward * curSpeed);
        }

        public void MoveBackwards(float param)
        {
            _forward = transform.TransformDirection(Vector3.forward);

            float curSpeed = -_moveSpeed * param;
            _characterController.SimpleMove(_forward * curSpeed);

        }

        public void RotateLeft(float param)
        {
            transform.Rotate(0, -_rotateSpeed * param, 0);

        }

        public void RotateRight(float param)
        {
            transform.Rotate(0, _rotateSpeed * param, 0);

        }

        #endregion
        #region public properties
        public LayerMask EnemyLayers
        {
            get { return _enemyLayers; }
        }
        public CharacterStates CharacterState
        {
            get { return _actions.CharacterState; }
        }
        public Characteristics Characteristics
        {
            get { return _characteristics; }
        }
        public float MoveSpeed
        {
            get { return _moveSpeed; }
            set { _moveSpeed = value; }
        }
        public float RotationSpeed
        {
            get { return _rotateSpeed; }
            set { _rotateSpeed = value; }
        }
        #endregion
        #region public methods
        // получение урона персонажем
        public bool Hit(int damage)
        {

            if (_actions.CharacterState == CharacterStates.DEAD) return false;

            // высчитываем какой урон должен получить персонаж

            // по идее персонаж в состоянии защиты должен получать меньше урона
            // но сейчас счтитаем, что урон одинаковый
            if (_actions.CharacterState == CharacterStates.DEFENCE)
            {
                damage = damage - (int)(_characteristics.Defence * Characteristics.DeltaDefence);

            }
            else
            {
                damage = damage - (int)(_characteristics.Defence * Characteristics.DeltaDefence);
            }
            // проверяем, возможно защита персонажа поглотила весь урон
            if (damage > 0)
            {
                _characteristics.Health -= damage;

            }
            // если здоровя у персонажа не осталось, он умер
            if (_characteristics.Health <= 0)
            {
                Dead();
                return true;
            }

            return false;

        }

        #endregion
    }
}
