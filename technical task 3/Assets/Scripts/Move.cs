﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum CharacterStates { IDLE, WALK, ATTACK, DEFENCE, DIED }
namespace TechTask3
{
    public class Move : MonoBehaviour
    {

        #region private fields
        [SerializeField] float _speed = 3.0f;
        [SerializeField] float _rotateSpeed = 3.0f;
        private CharacterController _controller = null;

        private CharacterStates _playerState = CharacterStates.IDLE;
        private float _hor;
        private float _ver;
        private Vector3 _forward;
        #endregion
        #region private methods
        private void Awake()
        {
            _controller = GetComponent<CharacterController>();

        }

        // Update is called once per frame
        void Update()
        {



            if (Input.GetButtonDown("Fire1"))
            {
                PlayerState = CharacterStates.ATTACK;
                return;
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                PlayerState = CharacterStates.IDLE;
                return;
            }

            if (Input.GetButtonDown("Fire2"))
            {
                PlayerState = CharacterStates.DEFENCE;
                return;
            }
            else if (Input.GetButtonUp("Fire2"))
            {
                PlayerState = CharacterStates.IDLE;
                return;
            }

            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S))
            {

                PlayerState = CharacterStates.WALK;
            }
            else if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
            {
                PlayerState = CharacterStates.IDLE;
            }
            _hor = Input.GetAxis("Horizontal");
            _ver = Input.GetAxis("Vertical");
            transform.Rotate(0, _hor * _rotateSpeed, 0);

            _forward = transform.TransformDirection(Vector3.forward);

            float curSpeed = _speed * _ver;
            _controller.SimpleMove(_forward * curSpeed);


        }
        void SetAnimationParamBool(string param, bool flag)
        {
            /*
            if (_animator)
            {
                _animator.SetBool(param, flag);

            }
            */
        }
        void setState(bool flag)
        {
            switch (_playerState)
            {
                case CharacterStates.ATTACK:
                    SetAnimationParamBool("Attack", flag);
                    break;
                case CharacterStates.WALK:
                    SetAnimationParamBool("Walk", flag);
                    break;
                case CharacterStates.IDLE:
                    //SetAnimationParamBool("Idle", flag);
                    break;
                case CharacterStates.DEFENCE:
                    SetAnimationParamBool("Defence", flag);
                    break;

            }

        }
        private CharacterStates PlayerState
        {
            get { return _playerState; }
            set
            {
                setState(false);
                _playerState = value;
                setState(true);
            }

        }

        #endregion
    }
}