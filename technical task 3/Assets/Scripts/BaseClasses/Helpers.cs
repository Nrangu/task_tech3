﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Base
{
    public static class  Helpers
    {

        // передавать не номер слоя, а именно маску
        // так как gameobject хранит именно номер слоя
        // но нужно параметр передавать так 1 << gameobject.layer
        //будем считать, что если layer2 == 0
        // то layer1 находится в layer2
        public static bool inLayer(LayerMask layer1, LayerMask layer2)
        {

            if (layer2.value == 0) return true;
            if ((layer1.value & layer2.value) == 0 && layer2.value != 0)
            {
                return false;
            }
            return true;
        }

    }
}
