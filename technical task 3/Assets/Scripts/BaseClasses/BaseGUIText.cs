﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base;

namespace Base
{
    public class BaseGUIText :BaseGUI
    {

        #region private fields
        private string _textLeft ;
        private string _textRight;
        private Text _textComponent = null;
        #endregion

        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            _textComponent = GetComponent<Text>();
        }
        #endregion
        #region public properties
        public string Text
        {
            get {
                if (_textComponent)
                {
                    return _textComponent.text;
                } 
                return "";
            }
            set
            {
                if (_textComponent)
                {
                    _textComponent.text = value;
                }
            }
        }
        public string TextLeft
        {
            get { return _textLeft; }
            set
            {
                _textLeft = value;
                if (_textComponent)
                {
                    _textComponent.text = _textLeft + _textRight;
                }
            }
        }
        public string TextRight
        {
            get { return _textRight; }
            set
            {
                _textRight = value;
                if (_textComponent)
                {
                    _textComponent.text = _textLeft + _textRight;
                }
            }
        }
        public Text TextComponent
        {
            get { return _textComponent; }
            //set { _textComponent = value; }
        }
        #endregion
    }

}
