﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Base
{
    public class BaseGUI : MonoBehaviour
    {
        #region private fields
        GameObject _instanceObject;
        bool _enabled = false;
        bool _active = false;
        #endregion
        #region protected methods
        // Use this for initialization
        protected virtual void Awake()
        {
            _instanceObject = gameObject;
            ActiveObject = true;

        }

        #endregion
        #region public properties
        public GameObject InstanceObject
        {
            get { return _instanceObject; }
            protected set { _instanceObject = value; }
        }
        public bool EnabledObject
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public bool ActiveObject
        {
            get { return _active; }
            set
            {
                _active = value;
                if (_instanceObject)
                {
                    _instanceObject.SetActive(_active);
                }
            }
        }
        #endregion
    }
}