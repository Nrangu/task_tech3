﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Base
{
    public class SliderLine : BaseGUI
    {

        #region private fields
        Slider _healthLine;
        float _minValue = 0;
        float _maxValue = 0;
        float _currentValue = 0;
        #endregion
        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            ActiveObject = true;
            _healthLine = InstanceObject.GetComponent<Slider>();
            MaxValue = 0;
            MinValue = 0;
            Value = 0;

        }
        #endregion
        #region public properties
        public float MinValue
        {
            get { return _minValue; }
            set
            {
                _minValue = value;
                if (_healthLine)
                {
                    _healthLine.minValue = _minValue;
                }
            }
        }

        public float MaxValue
        {
            get { return _maxValue; }
            set
            {
                _maxValue = value;
                if (_healthLine)
                {
                    _healthLine.maxValue = _maxValue;
                }
            }
        }
        public float Value
        {
            get { return _currentValue; }
            set
            {
                _currentValue = value;

                if (_currentValue < _minValue)
                {
                    _currentValue = 0;
                }
                if (_currentValue > _maxValue)
                {
                    _currentValue = _maxValue;
                }

                if (_healthLine)
                {
                    _healthLine.value = _currentValue;
                }
            }
        }
        #endregion
    }
}
