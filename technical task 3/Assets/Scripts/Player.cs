﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Base;

namespace TechTask3
{
    //public enum CharacterStates { IDLE, WALK, ATTACK, DEFENCE, DIED }

    public class Player : BaseSceneObject
    {

        #region private fields
        [SerializeField] private int _minAttack = 0;
        [SerializeField] private int _maxAttack = 0;
        [SerializeField] private int _defence = 0;
        [SerializeField] private SliderLine _healthLine = null;
        [SerializeField] private SliderLine _staminaLine = null;
        [SerializeField] private SliderLine _manaLine = null;
        private Coroutine _coroutineHealth = null;
        private Coroutine _coroutineMana = null;
        private Coroutine _coroutineStamina = null;

        [SerializeField] private MobileController _joystick = null;

        [SerializeField] private int _deltaHealthRegen = 0; // на скольо повышается значение здоровья 
        [SerializeField] private float _timeHealthRegen = 0; // с какой скоростью восстаналивается здороье

        [SerializeField] private int _deltaManaRegen = 0;
        [SerializeField] private float _timeManaRegen = 0;

        [SerializeField] private int _deltaStaminaRegen = 0;
        [SerializeField] private float _timeStaminaRegen = 0;
        private Swipe _rotateSwipe = null;

        private CharacterActions  _actions = null;

        private Character _character = null;
        bool _swipe = false;
        #endregion

        #region protected methods
        protected override void Awake()
        {
            base.Awake();
             _actions = GetComponent<CharacterActions>();
            if (! _actions)
            {
                 _actions = InstanceObject.AddComponent<CharacterActions>();
            }


            _character = GetComponent<Character>();
            _rotateSwipe = GetComponent<Swipe>();
        }
        #endregion
        #region private methods
        // Use this for initialization
        void Start()
        {
            _character.Characteristics.MinAttack = _minAttack;
            _character.Characteristics.MaxAttack = _maxAttack;
            _character.Characteristics.Defence = _defence;

            _character.Characteristics.OnHealthChange += OnHealthChange;
            _character.Characteristics.OnMaxHealthChange += OnMaxHealthChange;

            _character.Characteristics.OnStaminaChange += OnStaminaChange;
            _character.Characteristics.OnMaxStaminaChange += OnMaxStaminaChange;

            _character.Characteristics.OnManaChange += OnManaChange;
            _character.Characteristics.OnMaxManaChange += OnMaxManaChange;

            if (_healthLine)
            {
                _healthLine.MinValue = 0;
                _healthLine.MaxValue = _character.Characteristics.MaxHealth;
                _healthLine.Value = _healthLine.MaxValue;
            }

            if (_staminaLine)
            {
                _staminaLine.MinValue = 0;
                _staminaLine.MaxValue = _character.Characteristics.MaxStamina;
                _staminaLine.Value = _character.Characteristics.MaxStamina;
            }

            if (_manaLine)
            {
                _manaLine.MinValue = 0;
                _manaLine.MaxValue = _character.Characteristics.MaxMana;
                _manaLine.Value = _character.Characteristics.MaxMana;
            }

            if (_joystick)
            {
                _joystick.EventPoinerUp += delegate () { _actions.CharacterState = CharacterStates.IDLE; };
                _joystick.EventDrag += delegate ()
                {
                    if (_joystick.Vertical() > 0)
                    {
                        _actions.CharacterState = CharacterStates.WALK;
                    }
                    if (_joystick.Vertical() < 0)
                    {
                        _actions.CharacterState = CharacterStates.WALKBACKWARDS;
                    }


                };
            }
            if (_character)
            {
                _character.OnHitEvent += delegate (ForHit forHit)
                {
                    // сохраняем уровень сразу, так как при переходе
                    // в состояние смерти враг объет может быть уничтожен
                    int enemyLevel =forHit.Level;
                    // проверяем не привела ли атака к смерти врага
                    if (forHit.OnHit(_character.Attack()))
                    {
                        _character.Characteristics.Eperiance += enemyLevel * _character.Characteristics.DeltaExpereanceForKill;
                    };

                };
            }

            _coroutineHealth = StartCoroutine(HealthRegen());
            _coroutineMana = StartCoroutine(ManaRegen());
            _coroutineStamina =  StartCoroutine(StaminaRegen());
        }
        // Update is called once per frame
        void Update()
        {
            //Debug.Log("BaseForse " + _character.Characteristics.BaseForce);
            /*
           if (Input.GetButtonDown("Fire1"))
            {
                 _actions.CharacterState = CharacterStates.ATTACK;
                return;
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                 _actions.CharacterState = CharacterStates.IDLE;
                return;
            }
            */
            /*
                        if (Input.GetButtonDown("Fire2"))
                        {
                             _actions.CharacterState = CharacterStates.DEFENCE;
                            return;
                        }
                        else if (Input.GetButtonUp("Fire2"))
                        {
                             _actions.CharacterState = CharacterStates.IDLE;
                            return;
                        }
                        */
            //if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S))
            //{

            //     _actions.CharacterState = CharacterStates.WALK;
            //}
            //else if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
            //{
            //     _actions.CharacterState = CharacterStates.IDLE;
            //}        }

            // _character.MoveForward(Time.deltaTime);
            switch (_actions.CharacterState)
            {
                case CharacterStates.WALK:
                    if (_swipe)
                    {
                        if (_rotateSwipe)
                        {
                            _rotateSwipe.OnSwipe();
                            if (_rotateSwipe.SwipeHorizontal > 0)
                            {
                                _character.RotateLeft(Time.deltaTime);
                            }
                            if (_rotateSwipe.SwipeHorizontal < 0)
                            {
                                _character.RotateRight(Time.deltaTime);
                            }
                        }

                    }
                    _character.MoveForward(Time.deltaTime);
                    //Rotate();
                    break;
                case CharacterStates.WALKBACKWARDS:
                    if (_swipe)
                    {
                        if (_rotateSwipe)
                        {
                            _rotateSwipe.OnSwipe();
                            if (_rotateSwipe.SwipeHorizontal > 0)
                            {
                                _character.RotateLeft(Time.deltaTime);
                            }
                            if (_rotateSwipe.SwipeHorizontal < 0)
                            {
                                _character.RotateRight(Time.deltaTime);
                            }
                        }

                    }
                    _character.MoveBackwards(Time.deltaTime);
                    //Rotate();
                    break;
                case CharacterStates.ROTATELEFT:
                    //_character.RotateLeft(Time.deltaTime);
                    break;
                case CharacterStates.ROTATERIGHT:
                   // _character.RotateRight(Time.deltaTime);
                    break;
                case CharacterStates.IDLE:
                    if (_swipe)
                    {
                        if (_rotateSwipe)
                        {
                            _rotateSwipe.OnSwipe();
                            if (_rotateSwipe.SwipeHorizontal > 0)
                            {
                                _character.RotateLeft(Time.deltaTime);
                            }
                            if (_rotateSwipe.SwipeHorizontal < 0)
                            {
                                _character.RotateRight(Time.deltaTime);
                            }
                        }

                    }
                    break;
                case CharacterStates.DEAD:
                    StopCoroutine(_coroutineHealth);
                    StopCoroutine(_coroutineMana);
                    StopCoroutine(_coroutineStamina);

                    break;

            }
            /*
            if (_swipe)
            {
                if (_rotateSwipe)
                {
                    _rotateSwipe.OnSwipe();
                }

            }
            */
        }
        void Rotate()
        {
            if (_joystick)
            {
                if (_joystick.Horizontal() > 0 )
                {
                    _character.RotateRight(Time.deltaTime);
                }
                if (_joystick.Horizontal() < 0)
                {
                    _character.RotateLeft(Time.deltaTime);
                }

            }
        }
        IEnumerator HealthRegen()
        {
            while (true)
            {
                if (_character.Characteristics.Health < _character.Characteristics.MaxHealth && _actions.CharacterState != CharacterStates.DEAD)
                {
                    _character.Characteristics.Health += _deltaHealthRegen;
                    if (_healthLine) { 
                    _healthLine.Value = _character.Characteristics.Health;
                    }
                }
                yield return new WaitForSeconds(_timeHealthRegen);
            }
        }

        IEnumerator ManaRegen()
        {
            while (true)
            {
                if (_character.Characteristics.Mana < _character.Characteristics.MaxMana && _actions.CharacterState != CharacterStates.DEAD)
                {
                    _character.Characteristics.Mana += _deltaManaRegen;
                    if (_manaLine)
                    {
                        _manaLine.Value = _character.Characteristics.Mana;
                    }
                }
                yield return new WaitForSeconds(_timeManaRegen);
            }
        }
        IEnumerator StaminaRegen()
        {
            while (true)
            {
                if (_character.Characteristics.Stamina < _character.Characteristics.MaxStamina && _actions.CharacterState != CharacterStates.DEAD)
                {
                    _character.Characteristics.Stamina += _deltaStaminaRegen;
                    if (_staminaLine)
                    {
                        _staminaLine.Value = _character.Characteristics.Stamina;
                    }
                }
                yield return new WaitForSeconds(_timeStaminaRegen);
            }
        }

        #endregion

        #region public methods
        public void OnSwipeExit()
        {
            _swipe = false;
        }
        public void OnSwipeDown()
        {
            _swipe = true;
        }

        public void OnSwipeUp()
        {
            _swipe = false;
        }
        public void OnHealthChange()
        {
            _healthLine.Value = _character.Characteristics.Health;
        }
        public void OnMaxHealthChange()
        {
            _healthLine.MaxValue = _character.Characteristics.MaxHealth;
        }

        public void OnStaminaChange()
        {
            _staminaLine.Value = _character.Characteristics.Stamina;
        }

        public void OnMaxStaminaChange()
        {
            _staminaLine.MaxValue = _character.Characteristics.MaxStamina;
        }


        public void OnManaChange()
        {
            _manaLine.Value = _character.Characteristics.Mana;
        }

        public void OnMaxManaChange()
        {
            _manaLine.MaxValue = _character.Characteristics.MaxMana;
        }
        public void AttackDown()
        {
            if (_actions.CharacterState == CharacterStates.ATTACK) return;
             _actions.CharacterState = CharacterStates.ATTACK;
        }
        public void DefenceDown()
        {
             _actions.CharacterState = CharacterStates.DEFENCE;
        }
        public void DefenceUp()
        {
             _actions.CharacterState = CharacterStates.IDLE;
        }

        public void MoveForwardDown()
        {
            _actions.CharacterState = CharacterStates.WALK;
        }

        public void MoveForwardUp()
        {
            _actions.CharacterState = CharacterStates.IDLE;
        }

        public void MoveBackwardsDown()
        {
            _actions.CharacterState = CharacterStates.WALKBACKWARDS;
        }

        public void MoveBackwardsUp()
        {
            _actions.CharacterState = CharacterStates.IDLE;
        }

        public void RotateLeftDown()
        {
            _actions.CharacterState = CharacterStates.ROTATELEFT;
        }
        public void RotateLeftUp()
        {
            _actions.CharacterState = CharacterStates.IDLE;
        }
        public void RotateRightDown()
        {
            _actions.CharacterState = CharacterStates.ROTATERIGHT;
        }
        public void RotateRighttUp()
        {
            _actions.CharacterState = CharacterStates.IDLE;
        }
        #endregion
    }
}