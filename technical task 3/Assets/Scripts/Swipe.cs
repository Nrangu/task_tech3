﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace TechTask3
{
    //Работает тольо для перспективной камеры!

    public class Swipe : MonoBehaviour
    {
        #region private fields
        [SerializeField] private float _speedRotation = 0;
        [SerializeField] private Camera _camera;
        private Vector2 _startPos;
        private float _swipeHor = 0;
        private float _swipeVer = 0;
        #endregion
        #region public methods
        public void OnSwipe()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _startPos = _camera.ScreenToViewportPoint(Input.mousePosition);
            }
            else if (Input.GetMouseButton(0))
            {
                Vector2 currentPos = _camera.ScreenToViewportPoint(Input.mousePosition);
                _swipeHor = _startPos.x - currentPos.x;
                _swipeVer = _startPos.y - currentPos.y;
                _startPos = currentPos;
            }
            if (Input.GetMouseButtonUp(0))
            {
                _swipeHor = 0;
                _swipeVer = 0;
            }
        }
        #endregion
        #region public fields
        public float SwipeHorizontal
        {
            get { return _swipeHor; }
        }
        public float SwipeVertical
        {
            get { return _swipeVer; }
        }
        #endregion
    }
}