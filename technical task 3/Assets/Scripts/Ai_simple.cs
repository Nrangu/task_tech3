﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Base;

namespace TechTask3
{
    public class Ai_simple : MonoBehaviour
    {
        #region private fields
        [SerializeField] private NavMeshAgent _navMesh = null;
        [SerializeField] private Transform _object = null;
        [SerializeField] private Character _character = null;
        [SerializeField] private CharacterActions _actions = null;
        private CharacterActions _objectActions = null;
        private bool _isObjectDead = false;
        private float _stopDistance = 0;
        #endregion
        #region private methods
        // Use this for initialization
        void Start()
        {
            _navMesh = GetComponent<NavMeshAgent>();
           // _navMesh.SetDestination(_object.position);
            _character = GetComponent<Character>();
            _actions = GetComponent<CharacterActions>();

            if (_actions)
            {
                _actions.CharacterState = CharacterStates.IDLE;

            }
            if (_navMesh)
            {
                _stopDistance = _navMesh.stoppingDistance;
            }
            if (_object)
            {
                _objectActions = _object.GetComponent<CharacterActions>();
            }
            if (_navMesh && _character)
            {
                _navMesh.speed = _character.MoveSpeed;
                _navMesh.angularSpeed = _character.RotationSpeed;
            }
            //_navMesh.Resume

        }

        // Update is called once per frame
        void Update()
        {
            
            if (_objectActions.CharacterState == CharacterStates.DEAD)
            {
                if (_actions.CharacterState != CharacterStates.IDLE)
                {
                    _actions.CharacterState = CharacterStates.IDLE;
                    if (_navMesh)
                    {
                        _navMesh.isStopped = true;
                    }
                }
                return;
            }

            switch (_actions.CharacterState)
            {
                case CharacterStates.IDLE:
                    if (Vector3.Distance(_object.transform.position, transform.position) >= _stopDistance)
                    {
                        _actions.CharacterState = CharacterStates.WALK;
                        _navMesh.isStopped = false;
                        _navMesh.SetDestination(_object.position);
                    }else
                    {
                        _navMesh.isStopped = true;
                        _actions.CharacterState = CharacterStates.ATTACK;
                    }
                    break;
                case CharacterStates.WALK:
                    if (Vector3.Distance(_object.transform.position, transform.position) >= _stopDistance)
                    {
                        _actions.CharacterState = CharacterStates.WALK;
                        _navMesh.isStopped = false;
                        _navMesh.SetDestination(_object.position);
                    }else
                    {
                        _navMesh.isStopped = true;
                        _actions.CharacterState = CharacterStates.ATTACK;
                    }
                    break;
                case CharacterStates.ATTACK:
                    if (Vector3.Distance(_object.transform.position, transform.position) <= _stopDistance)
                    {
                        _actions.CharacterState = CharacterStates.ATTACK;
                    }
                    else
                    {
                        _actions.CharacterState = CharacterStates.WALK;
                    }

                    break;
                case CharacterStates.DEAD:
                    _navMesh.isStopped = true;
                    break;
            }
        }
        #endregion
        #region public properties
        public bool IsObjectDead
        {
            get { return _isObjectDead; }
            set { _isObjectDead = value; }
        }
        #endregion
    }
}