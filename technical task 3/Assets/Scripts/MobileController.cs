﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Base
{
    public delegate void eventDlg();
    public class MobileController : MonoBehaviour, IDragHandler, IPointerUpHandler,IPointerDownHandler
    {
        [SerializeField] private Image _joystickBG = null;
        [SerializeField] private Image _joystickCenter = null;
        private Vector2 _inputVector;
        #region public methods
        public event eventDlg EventPointerDown;
        public event eventDlg EventPoinerUp;
        public event eventDlg EventDrag;
        public virtual void OnPointerDown(PointerEventData ped)
        {
            OnDrag(ped);
            if (EventPointerDown != null)
            {
                EventPointerDown();
            }
        }
        public virtual void OnPointerUp(PointerEventData ped)
        {
            _inputVector = Vector2.zero;
            _joystickCenter.rectTransform.anchoredPosition = Vector2.zero;
            if (EventPoinerUp != null)
            {
                EventPoinerUp();
            }
        }
        public virtual void OnDrag(PointerEventData ped)
        {
            Vector2 pos;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_joystickBG.rectTransform,ped.position, ped.pressEventCamera, out pos))
            {
                pos.x = (pos.x / _joystickBG.rectTransform.sizeDelta.x);
                pos.y = (pos.y / _joystickBG.rectTransform.sizeDelta.y);

                _inputVector = new Vector2(pos.x * 2 , pos.y * 2 );
                _inputVector = (_inputVector.magnitude > 1.0f) ? _inputVector.normalized : _inputVector;

                _joystickCenter.rectTransform.anchoredPosition = new Vector2(_inputVector.x * (_joystickBG.rectTransform.sizeDelta.x / 2), _inputVector.y * (_joystickBG.rectTransform.sizeDelta.y / 2));


                if (EventDrag != null)
                {
                    EventDrag();
                }
            }
        }

        public float Horizontal()
        {
            if (_inputVector.x != 0) return _inputVector.x;
            return Input.GetAxis("Horizontal");
        }

        public float Vertical()
        {
            if (_inputVector.y != 0) return _inputVector.y;

            return Input.GetAxis("Vertical");
        }
        #endregion
    }
}