﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace TechTask3
{
    /// <summary>
    ///  Класс описывающие общие свойства для персонажа и для неигрового и для игрового(управляемого игроком)
    ///  Так же для каждого состояния можно задать действие через делегат без параметров
    /// </summary>
    ///

    // Возможные состояния персонажа

    public enum CharacterStates { IDLE, WALK,WALKBACKWARDS, ROTATELEFT, ROTATERIGHT,ATTACK, DEFENCE, DEAD }

    // Делегат который используется для передачи реакции на действие
    public delegate void actionForState();

    public class CharacterActions : MonoBehaviour
    {

        #region private fields


        private CharacterStates _characterState = CharacterStates.IDLE;

        private Dictionary<CharacterStates, actionForState> _actionDeactivate = new Dictionary<CharacterStates, actionForState>();
        private Dictionary<CharacterStates, actionForState> _actionActivate = new Dictionary<CharacterStates, actionForState>();
        #endregion

        #region private methods
       

        // Выполнить действие которое предусмотрено при деактивации текущего состояния
        private void DeactivateCurrentAction()
        {
            if (_actionDeactivate.ContainsKey(_characterState))
            {
                _actionDeactivate[_characterState]();
            }
        }

        // Выполнить действие которое предусмотрено при активации текущего состояния
        private void ActivateCurrentAction()
        {
            if (_actionActivate.ContainsKey(_characterState))
            {
                _actionActivate[_characterState]();
            }
        }

        #endregion
        #region public methods

        public void SeActivateAction(CharacterStates _state, actionForState _action)
        {
            _actionActivate.Add(_state,_action);
        }

        public void SeDeactivateAction(CharacterStates _state, actionForState _action)
        {
            _actionDeactivate.Add(_state,_action);
        }


        public void DoAction(CharacterStates _state)
        {
            if (_actionActivate.ContainsKey(_state))
            {
                _actionActivate[_state]();
            }
        }

        public void DoActionCurrentState()
        {
            if (_actionActivate.ContainsKey(_characterState))
            {
                _actionActivate[_characterState]();
            }
        }

        #endregion

        #region public properties

        public CharacterStates CharacterState
        {
            get { return _characterState; }
            set
            {
                if (_characterState == CharacterStates.DEAD) return;
                DeactivateCurrentAction();
                _characterState = value;
                ActivateCurrentAction();
            }
        }
        #endregion
    }
}