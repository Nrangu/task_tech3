﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;
namespace TechTask3
{
    public class SearchEnemy : MonoBehaviour
    {

        #region private fields
        private Character _character = null;
        #endregion
        #region private methods
        // Use this for initialization
        void Start()
        {
            _character = GetComponent<Character>();
        }
/*
        private void OnTriggerEnter(Collider other)
        {
            if (_character.CharacterState == CharacterStates.ATTACK) return;
            if (Helpers.inLayer( 1<<other.gameObject.layer, _character.EnemyLayers))
            {
                Enemy _enemy = other.gameObject.GetComponent<Enemy>();
                if (_enemy)
                {
                    _enemy.ObjectFinded(true);
                }
            }
        }
        */
        private void OnTriggerStay(Collider other)
        {
            if (_character.CharacterState == CharacterStates.ATTACK) return;
            if (Helpers.inLayer(1 << other.gameObject.layer, _character.EnemyLayers))
            {
                Enemy _enemy = other.gameObject.GetComponent<Enemy>();
                if (_enemy)
                {
                    _enemy.ObjectFinded(true);
                }
            }

        }
        private void OnTriggerExit(Collider other)
        {
            if (_character.CharacterState == CharacterStates.ATTACK) return;
            if (Helpers.inLayer(1 << other.gameObject.layer, _character.EnemyLayers))
            {
                Debug.Log(this.name);
                Debug.Log("Enemy exit " + other.name);
                Enemy _enemy = other.gameObject.GetComponent<Enemy>();
                if (_enemy)
                {
                    _enemy.ObjectFinded(false);
                }
            }

        }
        #endregion

    }
}